"""A module for calculating molar thermodynamic quantities of gases
using standard equations of statistical thermodynamics.
"""


import numpy as np
import molgemtools.geom as mg
import molgemtools.constants as constants


class Thermo:
    """Creates an object that represents the statistical thermodynamic
    properties of a gas.

    Parameters
    ----------
    x_dict : dict
        A dict containing molecular geometry information.

        x_dict['n'] : int
            The number of atoms.
        x_dict['name'] : str
            Comment line.
        x_dict['atoms'] : list of str
            The chemical symbols of the atoms in order.
        x_dict['xyz'] : ndarray
            The 3-by-n matrix of the atomic coordinates.
    scale : int or float, optional
        A constant for scaling the matrix of atomic coordinates.
    l_unit : str, optional
        The unit of length. Available options are: 'ang', 'm', 'cm'
        and 'a' corresponding to angstrom, metre, centimetre and Bohr
        radius.
    m_unit : str, optional
        The unit of mass. Available options are: 'u', 'kg', 'g', 'm_e'
        and 'm_p' corresponding to the atomic mass unit, kilogram,
        gram, electron mass and proton mass.
    symmetry : int, optional
        Rotational symmetry number.
    vibs : array_like or None, optional
        An array of the molar normal mode vibrational energies.
    temp : int or float, optional
        The temperature of the system in K.
    press : int or float, optional
        The pressure of the system in Pa.

    References
    ----------
    .. [Atkins2014] P. W. Atkins; J. De Paula; Physical Chemistry,
        Oxford University Press, 2014.

    Examples
    --------
    >>> import molgemtools.geom as mg
    >>> import statchem.thermo as thermo
    >>> NH3_vibs = [13178.66060359,
    ...             21128.68327624,
    ...             21130.35804816,
    ...             44085.61935513,
    ...             45669.47508419,
    ...             45674.73865308]
    >>> NH3 = thermo.Thermo(mg.open_xyz('data/NH3.xyz'),
    ...                     symmetry=3,
    ...                     vibs=NH3_vibs)
    """

    def __init__(self,
                 x_dict,
                 scale=10**-10,
                 l_unit="m",
                 m_unit="kg",
                 symmetry=1,
                 vibs=None,
                 temp=298.15,
                 press=101325):
        self.x_dict = x_dict
        self.scale = scale
        self.l_unit = l_unit
        self.m_unit = m_unit
        self.__rcond = 10**10
        self.__amu = constants.Constants.u
        self.__ang = constants.Constants.ang
        self.__m_u = mg.Geom.m_unit_dict[self.m_unit]
        self.__l_u = mg.Geom.l_unit_dict[self.l_unit]
        self.geometry = mg.Geom(self.x_dict,
                                scale=self.scale,
                                l_unit=self.l_unit,
                                m_unit=self.m_unit)
        self.num_of_atoms = self.geometry.n
        self.mass = self.geometry.mass
        self.inertia = self.geometry.inertia()
        # Avogadro constant in 1/mol.
        self.n_a = constants.Constants.n_a
        # Speed of light in vacuum in l_unit/s.
        self.c = constants.Constants.c*self.__l_u/self.__ang
        # Planck constant in m_unit*l_unit**2/s.
        self.h = constants.Constants.h*(self.__m_u*self.__l_u**2
                                        /(self.__amu*self.__ang**2))
        # Boltzmann constant in m_unit*l_unit**2/(s**2*K).
        self.k = constants.Constants.k*(self.__m_u*self.__l_u**2
                                        /(self.__amu*self.__ang**2))
        # Gas constant.
        self.r = self.n_a*self.k
        self.is_monatomic = self.num_of_atoms == 1
        self.is_linear = self.inertia.min()*self.__rcond < self.inertia.max()
        self.set_symmetry(symmetry)
        self.set_vibrations(vibs)
        self.set_temperature(temp)
        self.set_pressure(press)

    def set_symmetry(self, symmetry):
        """Sets the rotational symmetry number.

        Parameters
        ----------
        symmetry : int
        """
        self.symmetry = symmetry
        if not isinstance(self.symmetry, int):
            raise TypeError("Expected an int.")
        if self.symmetry < 1:
            raise ValueError("The symmetry number should be at least one.")

    def set_vibrations(self, vibs):
        """Sets the vibrational energies.

        Parameters
        ----------
        vibs : array_like
        """
        self.vibs = vibs
        if self.vibs is not None:
            self.vibs = np.array(vibs).flatten()
            if self.vibs.min() <= 0:
                raise ValueError("Vibrational energies are must be positive.")

    def set_temperature(self, temp):
        """Sets the temperature of the system in K.

        Parameters
        ----------
        temp : int or float
        """
        self.temp = float(temp)
        self.rt = self.r*self.temp
        if self.temp <= 0:
            raise ValueError("The temperature must be positive.")

    def set_pressure(self, press):
        """Sets the pressure of the system in Pa.

        Parameters
        ----------
        press : int or float
        """
        self.press = press*(self.__m_u/self.__l_u)/(self.__amu/self.__ang)
        if self.press <= 0:
            raise ValueError("The pressure must be positive.")

    def zero_point_energy(self):
        r"""Calculates the molar zero-point vibrational energy.

        Notes
        -----
        The zero-point vibrational energy can be defined as:

        .. math::
            E_m^{ZPV}=\frac{1}{2}\sum\limits_iE_{m,i}^{vib}.

        Returns
        -------
        zpve : float

        Examples
        --------
        >>> Ar.zero_point_energy()
        Traceback (most recent call last):
        ValueError: Monatomic molecule.
        >>> round(CH2FCl.zero_point_energy(), 2)
        87027.13
        """
        if self.is_monatomic:
            raise ValueError("Monatomic molecule.")
        if self.vibs is not None:
            return 0.5*self.vibs.sum()
        raise ValueError("Vibrational energy levels are not specified.")

    def rotational_constants(self):
        r"""Calculates the rotational constants in 1/`l_unit`.

        Returns
        -------
        rot_constants : ndarray

        Notes
        -----
        The rotational constant :math:`B` of a linear molecule can be
        defined as:

        .. math::
            B=\frac{h}{8\pi^2cI_b},

        where :math:`I_b` is the principal moment of inertia. In case
        of a nonlinear molecule similar equations can be used to
        calculate the :math:`A` and :math:`C` rotational constants by
        replacing :math:`I_b` with :math:`I_a` and :math:`I_c`.

        Examples
        --------
        >>> Ar.rotational_constants()
        Traceback (most recent call last):
        ValueError: Monatomic molecule.
        >>> Cl2.rotational_constants().round(5)
        array([ 0.    , 23.6224, 23.6224])
        >>> CH2FCl.rotational_constants().round(5)
        array([140.8778 ,  19.02718,  17.3282 ])
        """
        rot_constants = np.zeros(3)
        if self.is_monatomic:
            raise ValueError("Monatomic molecule.")
        elif self.is_linear:
            rot_constants[1:] = self.h/(8*np.pi**2*self.c*self.inertia.max())
        else:
            rot_constants = self.h/(8*np.pi**2*self.c*self.inertia)
        return rot_constants

    def rotational_temperatures(self):
        r"""Returns the rotational temperatures.

        Returns
        -------
        theta_rot : ndarray

        Notes
        -----
        The rotational temperature :math:`\Theta_B` of a linear
        molecule can be defined as:

        .. math::
            \Theta_B=\frac{hcB}{k_B},

        where :math:`B` is the rotational constant. Similar equations
        can be written for the :math:`\Theta_A` and :math:`\Theta_C`
        rotational temperatures of a nonlinear molecule.

        Examples
        --------
        >>> Ar.rotational_temperatures()
        Traceback (most recent call last):
        ValueError: Monatomic molecule.
        >>> Cl2.rotational_temperatures().round(5)
        array([0.     , 0.33987, 0.33987])
        >>> CH2FCl.rotational_temperatures().round(5)
        array([2.02692, 0.27376, 0.24931])
        """
        return self.h*self.c*self.rotational_constants()/self.k

    def vibrational_temperatures(self):
        r"""Returns the vibrational temperatures.

        Returns
        -------
        theta_vib : ndarray

        Notes
        -----
        The :math:`\Theta^{vib}` vibrational temperature corresponding
        to a normal mode vibrational energy of a molecule can be
        defined as:

        .. math::
            \Theta^{vib}=\frac{E_{m}^{vib}}{R}.

        Examples
        --------
        >>> Cl2.vibrational_temperatures().round(2)
        array([849.17])
        >>> theta_vib = CH2FCl.vibrational_temperatures()
        >>> for theta in theta_vib:
        ...     print(round(theta, 2))
        592.16
        1163.05
        1557.2
        1693.81
        1941.59
        2124.63
        2306.6
        4714.28
        4840.59
        """
        if self.is_monatomic:
            raise ValueError("Monatomic molecule.")
        if self.vibs is not None:
            return self.vibs/self.r
        raise ValueError("Vibrational energy levels are not specified.")

    def translational_partition(self):
        r"""Calculates the translational partition function.

        Returns
        -------
        q_t : float

        Notes
        -----
        The molecular translational partition function of a particle
        with a mass of :math:`m` at a temperature of :math:`T` can be
        written as:

        .. math::
            q^{trans}=\frac{k_BT}{p\Lambda^3},

        where :math:`p` is the pressure and :math:`\Lambda` is the
        thermal de Broglie wavelength:

        .. math::
            \Lambda=\sqrt{\frac{h^2}{2\pi mk_BT}}.

        Examples
        --------
        >>> round(CH2FCl.translational_partition(), 2)
        22273230.81
        """
        # The thermal de Broglie wavelength of the particle.
        lam = self.h/(2*np.pi*self.mass*self.k*self.temp)**0.5
        return self.k*self.temp/(self.press*lam**3)

    def rotational_partition(self):
        r"""Calculating the rotational partition function.

        Returns
        -------
        q_r : float

        Notes
        -----
        The rotational partition function of a linear rotor according
        to the high temperature approximation:

        .. math::
            q^{lin,rot}=\frac{T}{\sigma\Theta_B},

        where :math:`\sigma` is the symmetry number and
        :math:`\Theta_B` is the rotational temperature. The partition
        function of a nonlinear rotor:

        .. math::
            q^{rot}
            =\frac{1}{\sigma}
             \sqrt{\frac{T^3\pi}{\Theta_A\Theta_B\Theta_C}}.

        Examples
        --------
        >>> Ar.rotational_partition()
        Traceback (most recent call last):
        ValueError: Monatomic molecule.
        >>> round(Cl2.rotational_partition(), 2)
        438.62
        >>> round(CH2FCl.rotational_partition(), 2)
        24533.06
        """
        theta = self.rotational_temperatures().max()
        if theta > self.temp:
            raise ValueError("Low temperature.")
        if self.is_linear:
            return self.temp/(self.symmetry*theta)
        theta_abc = self.rotational_temperatures().prod()
        return (self.temp**3*np.pi/theta_abc)**0.5/self.symmetry

    def vibrational_partition(self):
        r"""Calculating the vibrational partition function.

        Returns
        -------
        q_v : float

        Notes
        -----
        The vibrational partition function of a polyatomic molecule:

        .. math::
            q^{vib}
            =\prod\limits_i
             \frac{1}{1-e^{-\frac{E_{m,i}^{vib}}{RT}}}.

        Examples
        --------
        >>> Ar.vibrational_partition()
        Traceback (most recent call last):
        ValueError: Monatomic molecule.
        >>> round(CH2FCl.vibrational_partition(), 5)
        1.19672
        """
        if self.is_monatomic:
            raise ValueError("Monatomic molecule.")
        if self.vibs is not None:
            return (1/(1 - np.e**(-self.vibs/self.rt))).prod()
        raise ValueError("Vibrational energy levels are not specified.")

    def partition(self):
        r"""Calculating the total partition function.

        Returns
        -------
        partition : float

        Notes
        -----
        The total molecular partition function is the product of its
        contributions:

        .. math::
            q^{tot}=q^{trans}q^{rot}q^{vib}.

        In case of a monatomic molecule, the total partition function
        only has the translational contribution:

        .. math::
            q^{mon,tot}=q^{mon,trans}.

        Examples
        --------
        >>> round(Ar.partition(), 2)
        9924260.86
        >>> round(CH2FCl.partition(), 2)
        653925723886.51
        """
        if self.is_monatomic:
            return self.translational_partition()
        return (self.translational_partition()
                *self.rotational_partition()
                *self.vibrational_partition())

    def translational_energy(self):
        r"""Calculates the translational contribution of the molar
        internal energy.

        Returns
        -------
        u_trans_m : float

        Notes
        -----
        The translational contribution to the molar internal energy
        according to the equipartition theorem:

        .. math::
            \Delta U_m^{trans}=\frac{3}{2}RT.

        Examples
        --------
        >>> round(CH2FCl.translational_energy(), 2)
        3718.44
        """
        return 1.5*self.rt

    def rotational_energy(self):
        r"""Calculates the rotational contribution of the molar
        internal energy.

        Returns
        -------
        u_rot_m : float

        Notes
        -----
        The rotational contribution of a linear rotor at high
        temperatures:

        .. math::
            \Delta U_m^{lin,rot}=RT.

        The equation for a nonlinear rotor:

        .. math::
            \Delta U_m^{rot}=\frac{3}{2}RT.

        Examples
        --------
        >>> Ar.rotational_energy()
        Traceback (most recent call last):
        ValueError: Monatomic molecule.
        >>> round(Cl2.rotational_energy(), 2)
        2478.96
        >>> round(CH2FCl.rotational_energy(), 2)
        3718.44
        """
        if self.is_monatomic:
            raise ValueError("Monatomic molecule.")
        if self.is_linear:
            return self.rt
        return 1.5*self.rt

    def vibrational_energy(self):
        r"""Calculates the vibrational contribution of the molar
        internal energy.

        Returns
        -------
        u_vib_m : float

        Notes
        -----
        The total vibrational contribution is the sum of the
        contributions of the independent vibrational energies:

        .. math::
            \Delta U_m^{vib}
            =\sum\limits_i
             \frac{E_{m,i}^{vib}}{e^{\frac{E_{m,i}^{vib}}{RT}}-1}.

        Examples
        --------
        >>> round(CH2FCl.vibrational_energy(), 2)
        1147.7
        """
        if self.is_monatomic:
            raise ValueError("Monatomic molecule.")
        if self.vibs is not None:
            return (self.vibs/(np.e**(self.vibs/self.rt) - 1)).sum()
        raise ValueError("Vibrational energy levels are not specified.")

    def energy(self):
        r"""Calculates the molar internal energy.

        Returns
        -------
        u_m : float

        Notes
        -----
        The total internal energy of a polyatomic molecule is the sum
        of the contributions:

        .. math::
            \Delta U_m
            =\Delta U_m^{trans}
            +\Delta U_m^{rot}
            +\Delta U_m^{vib}.

        A monatomic molecule can only store thermal translational
        energy:

        .. math::
            \Delta U_m^{mon}
            =\Delta U_m^{mon,trans}.

        Examples
        --------
        >>> round(Ar.energy(), 2)
        3718.44
        >>> U_trans = CH2FCl.translational_energy()
        >>> U_rot = CH2FCl.rotational_energy()
        >>> U_vib = CH2FCl.vibrational_energy()
        >>> U = CH2FCl.energy()
        >>> round(U_trans + U_rot + U_vib, 2)
        8584.57
        >>> round(U, 2)
        8584.57
        """
        if self.is_monatomic:
            return self.translational_energy()
        return (self.translational_energy()
                + self.rotational_energy()
                + self.vibrational_energy())

    def enthalpy(self):
        r"""Calculates the molar enthalpy.

        Returns
        -------
        h_m : float

        Notes
        -----
        According to classical thermodynamics, the definition of
        enthalpy:

        .. math::
            \Delta H_m=\Delta U_m+RT.

        Examples
        --------
        >>> round(Ar.enthalpy(), 2)
        6197.39
        >>> round(Cl2.enthalpy(), 2)
        9110.69
        >>> round(HCl.enthalpy(), 2)
        26387.8
        >>> round(NH3.enthalpy(), 2)
        9989.27
        >>> U = CH2FCl.energy()
        >>> H = CH2FCl.enthalpy()
        >>> round(U + R*T, 2)
        11063.53
        >>> round(H, 2)
        11063.53
        """
        return self.energy() + self.rt

    def helmholtz_energy(self):
        r"""Calculates the molar Helmholtz free energy.

        Returns
        -------
        a_m : float

        Notes
        -----
        The definition of the molar Helmholtz free energy according to
        statistical mechanics:

        .. math::
            \Delta A_m=-RTln(eq^{tot}).

        The classical definition:

        .. math::
            \Delta A_m=\Delta U_m-T\Delta S_m.

        Examples
        --------
        >>> round(Ar.helmholtz_energy(), 2)
        -42416.18
        >>> round(Cl2.helmholtz_energy(), 2)
        -59778.79
        >>> round(HCl.helmholtz_energy(), 2)
        -177863.75
        >>> round(NH3.helmholtz_energy(), 2)
        -49734.69
        >>> U = CH2FCl.energy()
        >>> S = CH2FCl.entropy()
        >>> A = CH2FCl.helmholtz_energy()
        >>> round(U - T*S, 2)
        -69922.11
        >>> round(A, 2)
        -69922.11
        """
        return -self.rt*np.log(np.e*self.partition())

    def gibbs_energy(self):
        r"""Calculates the molar Gibbs free energy.

        Returns
        -------
        g_m : float

        Notes
        -----
        The definition of the molar Gibbs free energy according to
        statistical mechanics:

        .. math::
            \Delta G_m=-RTln(q^{tot}).

        The classical definitions of the Gibbs energy:

        .. math::
            \Delta G_m=\Delta H_m-T\Delta S_m,

            \Delta G_m=\Delta A_m+RT.

        Examples
        --------
        >>> round(Ar.gibbs_energy(), 2)
        -39937.22
        >>> round(Cl2.gibbs_energy(), 2)
        -57299.84
        >>> round(HCl.gibbs_energy(), 2)
        -170396.11
        >>> round(NH3.gibbs_energy(), 2)
        -47255.73
        >>> H = CH2FCl.enthalpy()
        >>> A = CH2FCl.helmholtz_energy()
        >>> S = CH2FCl.entropy()
        >>> G = CH2FCl.gibbs_energy()
        >>> round(H - T*S, 2)
        -67443.15
        >>> round(A + R*T, 2)
        -67443.15
        >>> round(G, 2)
        -67443.15
        """
        return -self.rt*np.log(self.partition())

    def translational_entropy(self):
        r"""Calculates the translational contribution of the molar
        entropy.

        Returns
        -------
        s_trans_m : float

        Notes
        -----
        The entropy of a monatomic gas or the translational
        contribution of a polyatomic molecule can be calculated with
        the Sackur--Tetrode equation from the translational energy and
        partition function:

        .. math::
            \Delta S_m^{trans}
            =\frac{\Delta U_m^{trans}}{T}+Rln(eq^{trans})
            =Rln(e^\frac{5}{2}q^{trans}).

        Examples
        --------
        >>> round(Ar.translational_entropy(), 5)
        154.73625
        >>> round(Cl2.translational_entropy(), 5)
        161.89225
        >>> round(HCl.translational_entropy(), 5)
        176.51889
        >>> round(NH3.translational_entropy(), 5)
        144.10323
        >>> round(CH2FCl.translational_entropy(), 5)
        161.45769
        """
        return self.r*np.log(np.e**2.5*self.translational_partition())

    def rotational_entropy(self):
        r"""Calculates the rotational contribution of the molar
        entropy.

        Returns
        -------
        s_rot_m : float

        Notes
        -----
        The rotational contribution to the entropy of a general
        polyatomic molecule:

        .. math::
            \Delta S_m^{rot}
            =\frac{\Delta U_m^{rot}}{T}+Rln(q^{rot}).

        Examples
        --------
        >>> Ar.rotational_entropy()
        Traceback (most recent call last):
        ValueError: Monatomic molecule.
        >>> round(Cl2.rotational_entropy(), 5)
        58.89658
        >>> round(HCl.rotational_entropy(), 5)
        42.24512
        >>> round(NH3.rotational_entropy(), 5)
        47.60689
        >>> round(CH2FCl.rotational_entropy(), 5)
        96.51243
        """
        return (self.rotational_energy()/self.temp
                + self.r*np.log(self.rotational_partition()))

    def vibrational_entropy(self):
        r"""Calculates the vibrational contribution of the molar
        entropy.

        Returns
        -------
        s_vib_m : float

        Notes
        -----
        The equation for calculating the vibrational entropy:

        .. math::
            \Delta S_m^{vib}
            =\frac{\Delta U_m^{vib}}{T}+Rln(q^{vib}).

        Examples
        --------
        >>> Ar.vibrational_entropy()
        Traceback (most recent call last):
        ValueError: Monatomic molecule.
        >>> round(Cl2.vibrational_entropy(), 5)
        1.95317
        >>> round(HCl.vibrational_entropy(), 5)
        0.33516
        >>> round(NH3.vibrational_entropy(), 5)
        0.29058
        >>> round(CH2FCl.vibrational_entropy(), 5)
        5.34256
        """
        return (self.vibrational_energy()/self.temp
                + self.r*np.log(self.vibrational_partition()))

    def entropy(self):
        r"""Calculates the molar entropy.

        Returns
        -------
        s_m : float

        Notes
        -----
        The total entropy of a polyatomic molecule is the sum of its
        contributions:

        .. math::
            \Delta S_m
            =\Delta S_m^{trans}
            +\Delta S_m^{rot}
            +\Delta S_m^{vib}
            =\frac{\Delta U_m}{T}+Rln(eq^{tot}).

        A monatomic molecule only can translate:

        .. math::
            \Delta S_m^{mon}
            =\Delta S_m^{mon,trans}.

        Examples
        --------
        >>> round(Ar.entropy(), 5)
        154.73625
        >>> round(Cl2.entropy(), 5)
        222.74201
        >>> round(HCl.entropy(), 5)
        219.09917
        >>> round(NH3.entropy(), 5)
        192.0007
        >>> S_trans = CH2FCl.translational_entropy()
        >>> S_rot = CH2FCl.rotational_entropy()
        >>> S_vib = CH2FCl.vibrational_entropy()
        >>> S = CH2FCl.entropy()
        >>> round(S_trans + S_rot + S_vib, 5)
        263.31268
        >>> round(S, 5)
        263.31268
        """
        return self.energy()/self.temp + self.r*np.log(np.e*self.partition())

    def translational_heat_capacity(self):
        r"""Returns the molar translational heat capacity.

        Returns
        -------
        cv_trans_m : float

        Notes
        -----
        The equation of the translational heat capacity:

        .. math::
            \Delta C_{V,m}^{trans}=\frac{3}{2}R.

        Examples
        --------
        >>> round(Ar.translational_heat_capacity(), 5)
        12.47169
        >>> round(CH2FCl.translational_heat_capacity(), 5)
        12.47169
        """
        return 1.5*self.r

    def rotational_heat_capacity(self):
        r"""Returns the molar rotational heat capacity.

        Returns
        -------
        cv_rot_m : float

        Notes
        -----
        The rotational contribution to the heat capacity of a linear
        molecule at high temperatures:

        .. math::
            \Delta C_{V,m}^{lin,rot}=R.

        The equation for a nonlinear molecule:

        .. math::
            \Delta C_{V,m}^{rot}=\frac{3}{2}R.

        Examples
        --------
        >>> Ar.rotational_heat_capacity()
        Traceback (most recent call last):
        ValueError: Monatomic molecule.
        >>> round(Cl2.rotational_heat_capacity(), 5)
        8.31446
        >>> round(CH2FCl.rotational_heat_capacity(), 5)
        12.47169
        """
        theta = self.rotational_temperatures().max()
        if theta > self.temp:
            raise ValueError("Low temperature.")
        if self.is_linear:
            return self.r
        return 1.5*self.r

    def vibrational_heat_capacity(self):
        r"""Returns the molar vibrational heat capacity.

        Returns
        -------
        cv_vib_m : float

        Notes
        -----
        The equation for calculating the vibrational heat capacity in
        terms of vibrational temperatures:

        .. math::
            \Delta C_{V,m}^{vib}
            =\sum\limits_i
             R\left(\frac{\Theta_i^{vib}}{T}\right) ^2
             \frac{e^\frac{\Theta_i^{vib}}{T}}
                  {\left(e^\frac{\Theta_i^{vib}}{T}-1\right) ^2}.

        Examples
        --------
        >>> Ar.vibrational_heat_capacity()
        Traceback (most recent call last):
        ValueError: Monatomic molecule.
        >>> round(Cl2.vibrational_heat_capacity(), 5)
        4.40437
        >>> round(HCl.vibrational_heat_capacity(), 5)
        1.4102
        >>> round(NH3.vibrational_heat_capacity(), 5)
        1.4058
        >>> round(CH2FCl.vibrational_heat_capacity(), 5)
        11.95249
        """
        x = self.vibrational_temperatures()/self.temp
        return (self.r*x**2*np.e**x/(np.e**x - 1)**2).sum()

    def heat_capacity(self):
        r"""Returns the molar constant volume heat capacity.

        Returns
        -------
        cv_m : float

        Notes
        -----
        The total heat capacity is the sum of its contributions:

        .. math::
            \Delta C_{V,m}
            =\Delta C_{V,m}^{trans}
            +\Delta C_{V,m}^{rot}
            +\Delta C_{V,m}^{vib}.

        A monatomic molecule only can translate:

        .. math::
            \Delta C_{V,m}^{mon}
            =\Delta C_{V,m}^{mon,trans}.

        Examples
        --------
        >>> round(Ar.heat_capacity(), 5)
        12.47169
        >>> round(Cl2.heat_capacity(), 5)
        25.19052
        >>> round(HCl.heat_capacity(), 5)
        22.19636
        >>> round(NH3.heat_capacity(), 5)
        26.34918
        >>> round(CH2FCl.heat_capacity(), 5)
        36.89588
        """
        if self.is_monatomic:
            return self.translational_heat_capacity()
        return (self.translational_heat_capacity()
                + self.rotational_heat_capacity()
                + self.vibrational_heat_capacity())
