"""A script for running the doctests of statchem.thermo."""


import os
import sys
src = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
os.chdir(src + "/tests")
sys.path.insert(0, src)
import doctest
import molgemtools.geom as mg
import molgemtools.constants as constants
import statchem.thermo as thermo


r = constants.Constants.n_a*constants.Constants.k
temp = 298.15

Cl2_vib = [7060.359904]

HCl_vib = [37419.18973178]

NH3_vibs = [13178.66060359,
            21128.68327624,
            21130.35804816,
            44085.61935513,
            45669.47508419,
            45674.73865308]

CH2FCl_vibs = [4923.47056199,
               9670.13305997,
               12947.30282564,
               14083.15706638,
               16143.24615325,
               17665.1353213,
               19178.1721235,
               39196.72087061,
               40246.92249035]

Ar = thermo.Thermo(mg.open_xyz("data/Ar.xyz"))
Cl2 = thermo.Thermo(mg.open_xyz("data/Cl2.xyz"), symmetry=2, vibs=Cl2_vib)
HCl = thermo.Thermo(mg.open_xyz("data/HCl.xyz"), vibs=HCl_vib, temp=898.15)
NH3 = thermo.Thermo(mg.open_xyz("data/NH3.xyz"), symmetry=3, vibs=NH3_vibs)
CH2FCl = thermo.Thermo(mg.open_xyz("data/CH2FCl.xyz"), vibs=CH2FCl_vibs)

doctest.testmod(thermo, extraglobs={"Ar": Ar,
                                    "Cl2": Cl2,
                                    "HCl": HCl,
                                    "NH3": NH3,
                                    "CH2FCl": CH2FCl,
                                    "R": r,
                                    "T": temp})
