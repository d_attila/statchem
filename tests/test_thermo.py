"""A script for running the unit tests of statchem.thermo."""


import os
import sys
src = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
os.chdir(src + "/tests")
sys.path.insert(0, src)
import unittest
import molgemtools.geom as mg
import statchem.thermo as thermo


Ar_xyz = mg.open_xyz("data/Ar.xyz")
Cl2_xyz = mg.open_xyz("data/Cl2.xyz")
Cl2_vib = 7060.359904


class TestThermo(unittest.TestCase):

    def setUp(self):
        self.Ar = thermo.Thermo(Ar_xyz)
        self.Cl2 = thermo.Thermo(Cl2_xyz,
                                 symmetry=2,
                                 vibs=[Cl2_vib])
        self.Cl2_low_temp = thermo.Thermo(Cl2_xyz,
                                          symmetry=2,
                                          vibs=[Cl2_vib],
                                          temp=0.3)
        self.Cl2_no_vibs = thermo.Thermo(Cl2_xyz,
                                         symmetry=2)

    def test_set_symmetry(self):
        with self.assertRaises(ValueError):
            self.Cl2.set_symmetry(-1)
        with self.assertRaises(TypeError):
            self.Cl2.set_symmetry(2.0)

    def test_set_vibrations(self):
        with self.assertRaises(ValueError):
            self.Cl2.set_vibrations([0, 100, 200])

    def test_set_temperature(self):
        with self.assertRaises(ValueError):
            self.Cl2.set_temperature(0)

    def test_set_pressure(self):
        with self.assertRaises(ValueError):
            self.Cl2.set_pressure(0)

    def test_zero_point_energy(self):
        with self.assertRaises(ValueError):
            self.Ar.zero_point_energy()
        with self.assertRaises(ValueError):
            self.Cl2_no_vibs.zero_point_energy()

    def test_vibrational_temperatures(self):
        with self.assertRaises(ValueError):
            self.Ar.vibrational_temperatures()
        with self.assertRaises(ValueError):
            self.Cl2_no_vibs.vibrational_temperatures()

    def test_rotational_partition(self):
        with self.assertRaises(ValueError):
            self.Ar.rotational_partition()
        with self.assertRaises(ValueError):
            self.Cl2_low_temp.rotational_partition()

    def test_vibrational_partition(self):
        with self.assertRaises(ValueError):
            self.Ar.vibrational_partition()
        with self.assertRaises(ValueError):
            self.Cl2_no_vibs.vibrational_partition()

    def test_vibrational_energy(self):
        with self.assertRaises(ValueError):
            self.Ar.vibrational_energy()
        with self.assertRaises(ValueError):
            self.Cl2_no_vibs.vibrational_energy()

    def test_rotational_heat_capacity(self):
        with self.assertRaises(ValueError):
            self.Ar.rotational_heat_capacity()
        with self.assertRaises(ValueError):
            self.Cl2_low_temp.rotational_heat_capacity()

    def test_vibrational_heat_capacity(self):
        with self.assertRaises(ValueError):
            self.Ar.vibrational_heat_capacity()
        with self.assertRaises(ValueError):
            self.Cl2_no_vibs.vibrational_heat_capacity()


if __name__ == "__main__":
    unittest.main()
