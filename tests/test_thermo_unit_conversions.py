import os
import sys
src = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
os.chdir(src + "/tests")
sys.path.insert(0, src)
import unittest
import molgemtools.geom as mg
import molgemtools.constants as constants
import statchem.thermo as thermo


m_to_ang = 10**10
ang_to_m = 10**-10
m_to_cm = 100
cm_to_m = 0.01
a_to_ang = constants.Constants.a_to_ang
ang_to_a = 1/a_to_ang
m_to_a = m_to_ang*ang_to_a
a_to_m = 1/m_to_a
kg_to_g = 1000
g_to_kg = 0.001
u_to_kg = constants.Constants.u
kg_to_u = 1/u_to_kg
m_e_to_kg = constants.Constants.m_e
kg_to_m_e = 1/m_e_to_kg
m_p_to_kg = constants.Constants.m_p
kg_to_m_p = 1/m_p_to_kg

Cl2_xyz = mg.open_xyz("data/Cl2.xyz")
Cl2_vib = 7060.359904
Cl2_vib_ang = Cl2_vib*m_to_ang**2
Cl2_vib_cm = Cl2_vib*m_to_cm**2
Cl2_vib_a = Cl2_vib*m_to_a**2
Cl2_vib_u = Cl2_vib*kg_to_u
Cl2_vib_g = Cl2_vib*kg_to_g
Cl2_vib_m_e = Cl2_vib*kg_to_m_e
Cl2_vib_m_p = Cl2_vib*kg_to_m_p


class TestThermoUnits(unittest.TestCase):

    def setUp(self):
        self.Cl2 = thermo.Thermo(Cl2_xyz,
                                 symmetry=2,
                                 vibs=[Cl2_vib])
        self.Cl2_ang = thermo.Thermo(Cl2_xyz,
                                     scale=1,
                                     l_unit="ang",
                                     symmetry=2,
                                     vibs=[Cl2_vib_ang])
        self.Cl2_cm = thermo.Thermo(Cl2_xyz,
                                    scale=10**-8,
                                    l_unit="cm",
                                    symmetry=2,
                                    vibs=[Cl2_vib_cm])
        self.Cl2_a = thermo.Thermo(Cl2_xyz,
                                   scale=ang_to_a,
                                   l_unit="a",
                                   symmetry=2,
                                   vibs=[Cl2_vib_a])
        self.Cl2_u = thermo.Thermo(Cl2_xyz,
                                   m_unit="u",
                                   symmetry=2,
                                   vibs=[Cl2_vib_u])
        self.Cl2_g = thermo.Thermo(Cl2_xyz,
                                   m_unit="g",
                                   symmetry=2,
                                   vibs=[Cl2_vib_g])
        self.Cl2_m_e = thermo.Thermo(Cl2_xyz,
                                     m_unit="m_e",
                                     symmetry=2,
                                     vibs=[Cl2_vib_m_e])
        self.Cl2_m_p = thermo.Thermo(Cl2_xyz,
                                     m_unit="m_p",
                                     symmetry=2,
                                     vibs=[Cl2_vib_m_p])

    def test_energy_units(self):
        Cl2_energy = self.Cl2.energy()
        self.assertAlmostEqual(ang_to_m**2*self.Cl2_ang.energy(), Cl2_energy)
        self.assertAlmostEqual(cm_to_m**2*self.Cl2_cm.energy(), Cl2_energy)
        self.assertAlmostEqual(a_to_m**2*self.Cl2_a.energy(), Cl2_energy)
        self.assertAlmostEqual(u_to_kg*self.Cl2_u.energy(), Cl2_energy)
        self.assertAlmostEqual(g_to_kg*self.Cl2_g.energy(), Cl2_energy)
        self.assertAlmostEqual(m_e_to_kg*self.Cl2_m_e.energy(), Cl2_energy)
        self.assertAlmostEqual(m_p_to_kg*self.Cl2_m_p.energy(), Cl2_energy)


if __name__ == "__main__":
    unittest.main()
