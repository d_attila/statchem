.. toctree::
    :maxdepth: 2
    :caption: Contents:

========
statchem
========

statchem reference
==================

statchem.thermo
---------------

.. autoclass:: statchem.thermo.Thermo

.. raw:: latex

    \newpage

.. autofunction:: statchem.thermo.Thermo.set_symmetry

.. autofunction:: statchem.thermo.Thermo.set_vibrations

.. autofunction:: statchem.thermo.Thermo.set_temperature

.. autofunction:: statchem.thermo.Thermo.set_pressure

.. autofunction:: statchem.thermo.Thermo.zero_point_energy

.. raw:: latex

    \newpage

.. autofunction:: statchem.thermo.Thermo.rotational_constants

.. autofunction:: statchem.thermo.Thermo.rotational_temperatures

.. raw:: latex

    \newpage

.. autofunction:: statchem.thermo.Thermo.vibrational_temperatures

.. raw:: latex

    \newpage

.. autofunction:: statchem.thermo.Thermo.translational_partition

.. autofunction:: statchem.thermo.Thermo.rotational_partition

.. raw:: latex

    \newpage

.. autofunction:: statchem.thermo.Thermo.vibrational_partition

.. autofunction:: statchem.thermo.Thermo.partition

.. raw:: latex

    \newpage

.. autofunction:: statchem.thermo.Thermo.translational_energy

.. autofunction:: statchem.thermo.Thermo.rotational_energy

.. raw:: latex

    \newpage

.. autofunction:: statchem.thermo.Thermo.vibrational_energy

.. autofunction:: statchem.thermo.Thermo.energy

.. raw:: latex

    \newpage

.. autofunction:: statchem.thermo.Thermo.enthalpy

.. raw:: latex

    \newpage

.. autofunction:: statchem.thermo.Thermo.helmholtz_energy

.. raw:: latex

    \newpage

.. autofunction:: statchem.thermo.Thermo.gibbs_energy

.. raw:: latex

    \newpage

.. autofunction:: statchem.thermo.Thermo.translational_entropy

.. autofunction:: statchem.thermo.Thermo.rotational_entropy

.. raw:: latex

    \newpage

.. autofunction:: statchem.thermo.Thermo.vibrational_entropy

.. raw:: latex

    \newpage

.. autofunction:: statchem.thermo.Thermo.entropy

.. raw:: latex

    \newpage

.. autofunction:: statchem.thermo.Thermo.translational_heat_capacity

.. autofunction:: statchem.thermo.Thermo.rotational_heat_capacity

.. raw:: latex

    \newpage

.. autofunction:: statchem.thermo.Thermo.vibrational_heat_capacity

.. autofunction:: statchem.thermo.Thermo.heat_capacity
