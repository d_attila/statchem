.. toctree::
    :maxdepth: 2
    :caption: Contents:

Statchem
--------

A pdf version is available :download:`here <../../doc/latex/_build/latex/statchem.pdf>`.

statchem.thermo
---------------

.. autoclass:: statchem.thermo.Thermo

.. autofunction:: statchem.thermo.Thermo.set_symmetry

.. autofunction:: statchem.thermo.Thermo.set_vibrations

.. autofunction:: statchem.thermo.Thermo.set_temperature

.. autofunction:: statchem.thermo.Thermo.set_pressure

.. autofunction:: statchem.thermo.Thermo.zero_point_energy

.. autofunction:: statchem.thermo.Thermo.rotational_constants

.. autofunction:: statchem.thermo.Thermo.rotational_temperatures

.. autofunction:: statchem.thermo.Thermo.vibrational_temperatures

.. autofunction:: statchem.thermo.Thermo.translational_partition

.. autofunction:: statchem.thermo.Thermo.rotational_partition

.. autofunction:: statchem.thermo.Thermo.vibrational_partition

.. autofunction:: statchem.thermo.Thermo.partition

.. autofunction:: statchem.thermo.Thermo.translational_energy

.. autofunction:: statchem.thermo.Thermo.rotational_energy

.. autofunction:: statchem.thermo.Thermo.vibrational_energy

.. autofunction:: statchem.thermo.Thermo.energy

.. autofunction:: statchem.thermo.Thermo.enthalpy

.. autofunction:: statchem.thermo.Thermo.helmholtz_energy

.. autofunction:: statchem.thermo.Thermo.gibbs_energy

.. autofunction:: statchem.thermo.Thermo.translational_entropy

.. autofunction:: statchem.thermo.Thermo.rotational_entropy

.. autofunction:: statchem.thermo.Thermo.vibrational_entropy

.. autofunction:: statchem.thermo.Thermo.entropy

.. autofunction:: statchem.thermo.Thermo.translational_heat_capacity

.. autofunction:: statchem.thermo.Thermo.rotational_heat_capacity

.. autofunction:: statchem.thermo.Thermo.vibrational_heat_capacity

.. autofunction:: statchem.thermo.Thermo.heat_capacity
