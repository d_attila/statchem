"""A package for calculating statistical thermodynamic quantities."""


__author__ = "Attila Dékány"
__copyright__ = "2020-2021, Attila Dékány"
__contact__ = "dekanyattilaadam@gmail.com"
__version__ = "0.0.1"
